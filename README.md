<h1 align="center">
    <img src="./src/assets/icons/mars.png" width="30">
    My Mars
</h1>

###### This mobile app allows you to view and rate photos from the Curiosity rover, add them to your library of favorite photos.

<div align="center">
    <img src="https://media.giphy.com/media/sBgoxZQZEV03VGn8Mx/giphy.gif">
</div>



## Project setup
This progect use [expo](https://expo.io/). iOS platform required.
```
npm install
npm run expo
```

#### TODO
Need to add a transition to the next page with a photo, after the images end.
Need to add a full-screen view of the images in the gallery.
