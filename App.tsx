import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { Provider } from 'react-redux'
import AppLoading from 'expo-app-loading'
import { useFonts } from 'expo-font'

import store from './src/redux/store'
import Evaluating from './src/containers/ContainerEvaluating'
import Gallery from './src/containers/ContainerGallery'

const Stack = createStackNavigator()

export default function App() {
  let [fontsLoaded] = useFonts({
    'IBMPlexSansMedium': require('./src/assets/fonts/IBMPlexSans-Medium.ttf'),
  })

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName={'Evaluating'} headerMode={'none'}>
          <Stack.Screen name={'Evaluating'} component={Evaluating} />
          <Stack.Screen name={'Gallery'} component={Gallery} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}
