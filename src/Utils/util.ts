import { AxiosResponse, AxiosError } from 'axios'
import { ICard, IError, IICallPostNasaRoverPhotosData, IPhoto, IAxiosErrorData } from '../app'

export const convertCallPostNasaRoverPhotosResponse = (response: AxiosResponse<IICallPostNasaRoverPhotosData>): ICard[] => {
    return response.data.photos.map((photo: IPhoto): ICard => {
        return {
            id: photo.id.toString(),
            imageUrl: photo.img_src,
            title: photo.rover.name,
            description: photo.camera.full_name,
            date: new Date(photo.earth_date),
        }
    })
}

export const convertCallPostError = (error: AxiosError<IAxiosErrorData>): IError => {
    if (error.response && error.response.data && error.response.data.error) {
        return {
            title: error.response.data.error.code ? error.response.data.error.code : '',
            message: error.response.data.error.message ? error.response.data.error.message : '',
            status: typeof error.response.status === 'number' ? error.response.status : 404,
        }
    }
    return {
        title: 'Unknown network error',
        message: '',
        status: 404,
    }
}
