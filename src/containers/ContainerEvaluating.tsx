import React from 'react'
import { connect } from 'react-redux'
import { IEvaluatingProps, IAppState, ICard, Enums, INavigation, IError } from '../app'
import * as thunkEvaluating from '../thunk/ThunkEvaluating'
import Evaluating from '../components/Evaluating'

const ContainerEvaluating: React.FC<IProps> = (props: IProps): React.ReactElement<IEvaluatingProps> => {
    React.useEffect(() => {
        props.performContainerInit()
    }, [])
    return (
        <Evaluating
            cardList={props.cardList}
            selectedCardIdList={props.selectedCardIdList}
            navigation={props.navigation}
            addCardToSelected={props.addCardToSelected}
            removeCardFromSelected={props.removeCardFromSelected}
            callPostNasaRoverPhotosPhase={props.callPostNasaRoverPhotosPhase}
            setDisplayedCardIndex={props.setDisplayedCardIndex}
            displayedCardIndex={props.displayedCardIndex}
            callPostNasaRoverPhotosError={props.callPostNasaRoverPhotosError}
        />
    )
}

interface IStateProps {
    cardList: ICard[],
    selectedCardIdList: string[],
    callPostNasaRoverPhotosPhase: Enums.IPhase,
    displayedCardIndex: number,
    callPostNasaRoverPhotosError: IError | null,
}

const mapStateToProps = (state: IAppState): IStateProps => {
    return {
        cardList: state.cardList,
        selectedCardIdList: state.selectedCardIdList,
        callPostNasaRoverPhotosPhase: state.callPostNasaRoverPhotosPhase,
        displayedCardIndex: state.displayedCardIndex,
        callPostNasaRoverPhotosError: state.callPostNasaRoverPhotosError,
    }
}

interface IDispatchProps {
    addCardToSelected: (cardId: string) => void,
    removeCardFromSelected: (cardId: string) => void,
    performContainerInit: () => void,
    setDisplayedCardIndex: (newCardIndex: number) => void,
}

interface IOwnProps {
    navigation: INavigation,
}

interface IProps extends IStateProps, IDispatchProps, IOwnProps { }

const mapDispatchToProps = (dispatch: Function): IDispatchProps => {
    return {
        addCardToSelected: (cardId: string): void => {
            dispatch(thunkEvaluating.addCardToSelected(cardId))
        },
        removeCardFromSelected: (cardId: string): void => {
            dispatch(thunkEvaluating.removeCardFromSelected(cardId))
        },
        performContainerInit: (): void => {
            dispatch(thunkEvaluating.performContainerInit())
        },
        setDisplayedCardIndex: (newCardIndex: number): void => {
            dispatch(thunkEvaluating.setDisplayedCardIndex(newCardIndex))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainerEvaluating)
