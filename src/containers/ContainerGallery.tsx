import React from 'react'
import { connect } from 'react-redux'
import { IGalleryProps, IAppState, ICard, Enums, INavigation } from '../app'
import Gallery from '../components/Gallery'
import { selectedCardsSelector } from '../selectors/GallerySelector'

const ContainerGallery: React.FC<IProps> = (props: IProps): React.ReactElement<IGalleryProps> => {
    return (
        <Gallery
            cardList={props.cardList}
            navigation={props.navigation}
        />
    )
}

interface IStateProps {
    cardList: ICard[],
}

const mapStateToProps = (state: IAppState): IStateProps => {
    return {
        cardList: selectedCardsSelector(state),
    }
}

interface IDispatchProps {
}

interface IOwnProps {
    navigation: INavigation,
}

interface IProps extends IStateProps, IDispatchProps, IOwnProps { }

const mapDispatchToProps = (dispatch: Function): IDispatchProps => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainerGallery)
