import React, { useState } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    Image,
    StyleProp,
    TextStyle,
    ImageSourcePropType,
    SafeAreaView,
} from 'react-native'
import styles from './styles/EvaluatingStyles'
import Cards from './common/Cards'
import ProgressBar from './common/ProgressBar'
import { Enums, ICard, IEvaluatingProps, INavigation } from '../app'

const heartIconSrc: ImageSourcePropType = require(`../assets/icons/heart.png`)
const heartDisabledIconSrc: ImageSourcePropType = require(`../assets/icons/heart-disabled.png`)

const CHANGE_CARD_TIMEOUT: number = 1000

const lockCardsForTimeout = (
    setIsCardsEnabled: (newState: boolean) => void,
    timeout: number,
): void => {
    setIsCardsEnabled(false)
    setTimeout(() => {
        setIsCardsEnabled(true)
    }, timeout)
}

const renderNavigationButton = (
    navigation: INavigation,
    callPostNasaRoverPhotosPhase: Enums.IPhase,
): React.ReactElement<Image | TouchableOpacity> => {
    if (callPostNasaRoverPhotosPhase === 'InProgress') {
        return (
            <Image
                source={heartDisabledIconSrc}
                style={styles.likeImage}
            />
        )
    }
    return (
        <TouchableOpacity
            onPress={() => navigation.navigate('Gallery')}
        >
            <Image
                source={heartIconSrc}
                style={styles.likeImage}
            />
        </TouchableOpacity>
    )
}

const renderBody = (
    props: IEvaluatingProps,
    setIsCardsEnabled: (newState: boolean) => void,
    isCardsEnabled: boolean
): React.ReactElement<View> => {
    if (props.callPostNasaRoverPhotosPhase === 'Failure') {
        if (props.callPostNasaRoverPhotosError) {
            return (
                <View style={styles.body}>
                    <View style={styles.errorContainer}>
                        <Text>{props.callPostNasaRoverPhotosError?.status}</Text>
                        <Text>{props.callPostNasaRoverPhotosError?.title}</Text>
                        <Text>{props.callPostNasaRoverPhotosError?.message}</Text>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={styles.body}>
                    <View style={styles.errorContainer}>
                        <Text>{'Network error'}</Text>
                        <Text>{'Try later'}</Text>
                    </View>
                </View>
            )
        }
    }
    if (props.callPostNasaRoverPhotosPhase === 'InProgress') {
        return (
            <View style={styles.body}>
                <View style={styles.loaderContainer}>
                    <ProgressBar />
                </View>
                <View style={styles.cardsLeftLabelContainer}>
                    <Text style={styles.caption}>{`Downloading`}</Text>
                </View>
            </View>
        )
    }
    const nextCardIndex: number = props.displayedCardIndex + 1
    const cardsLeft: number = props.cardList.length - props.displayedCardIndex

    const onDislike = (card: ICard): void => {
        lockCardsForTimeout(setIsCardsEnabled, CHANGE_CARD_TIMEOUT)
        props.setDisplayedCardIndex(nextCardIndex)
    }
    const onLike = (card: ICard): void => {
        lockCardsForTimeout(setIsCardsEnabled, CHANGE_CARD_TIMEOUT)
        props.addCardToSelected(card.id)
        props.setDisplayedCardIndex(nextCardIndex)
    }

    return (
        <View style={styles.body}>
            <Cards
                cardList={props.cardList}
                selectedCardIdList={props.selectedCardIdList}
                onDislike={onDislike}
                onLike={onLike}
                displayedCardIndex={props.displayedCardIndex}
                isEnabled={isCardsEnabled}
            />
            <View style={styles.cardsLeftLabelContainer}>
                <Text style={styles.caption}>{`${cardsLeft} ${cardsLeft === 1 ? 'card' : 'cards'}`}</Text>
            </View>
        </View>
    )
}

const Evaluating: React.FC<IEvaluatingProps> = (props: IEvaluatingProps) => {
    const [isCardsEnabled, setIsCardsEnabled] = useState<boolean>(true)

    const previousCardIndex: number = props.displayedCardIndex - 1

    const isHeaderButtonDisabled: boolean = previousCardIndex < 0 || !isCardsEnabled
    const headerButtonStyle: StyleProp<TextStyle> = [
        styles.headerButton,
        isHeaderButtonDisabled ? styles.headerDisabledButton : styles.headerEnabledButton,
    ]

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <View style={styles.leftHeaderContainer}>
                    <TouchableOpacity
                        onPress={() => {
                            if (previousCardIndex >= 0) {
                                lockCardsForTimeout(setIsCardsEnabled, CHANGE_CARD_TIMEOUT)
                                props.removeCardFromSelected(props.cardList[previousCardIndex].id)
                                props.setDisplayedCardIndex(previousCardIndex)
                            }
                        }}
                        disabled={isHeaderButtonDisabled}
                    >
                        <Text style={headerButtonStyle}>Undo</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.centerHeaderContainer}>
                    <Text style={styles.headerTitle}>My Mars</Text>
                </View>
                <View style={styles.rightHeaderContainer}>
                    {renderNavigationButton(props.navigation, props.callPostNasaRoverPhotosPhase)}
                </View>
            </View>
            {renderBody(props, setIsCardsEnabled, isCardsEnabled)}
        </SafeAreaView>
    )
}

export default Evaluating
