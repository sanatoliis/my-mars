import React from 'react'
import { View, Text, Image, ImageSourcePropType } from 'react-native'
import Animated, {
    event,
    set,
    cond,
    eq,
    call,
    Easing,
    block,
    Clock,
    startClock,
    clockRunning,
    stopClock,
    add,
    spring,
    sub,
    greaterThan,
    lessThan,
    multiply,
    divide,
    interpolate,
    Extrapolate,
} from 'react-native-reanimated'
import {
    PanGestureHandler,
    PanGestureHandlerGestureEvent,
    State as GestureState,
} from 'react-native-gesture-handler'
import { ICardsContainerDimensions } from './Cards'
import styles from './styles/CardStyles'
import { ICard } from '../../app'

const gradientImage: ImageSourcePropType = require('../../assets/icons/card-gradient.png')

const dateOptions: Intl.DateTimeFormatOptions = {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
}

const NUMBER_OF_CARDS_DISPLAYED: number = 3
const VERTICAL_OFFSET_BETWEEN_CARDS: number = 16
const HORIZONTAL_OFFSET_BETWEEN_CARDS: number = 16
const HORIZONTAL_MOVE_GAP: number = 100
const LEFT_OUT_SCREEN_VALUE: number = -1000
const RIGHT_OUT_SCREEN_VALUE: number = 1000

const extremeXValues: number[] = [
    LEFT_OUT_SCREEN_VALUE,
    0,
    RIGHT_OUT_SCREEN_VALUE
]

const getMinimumValue = (valueList: number[]): number => {
    if (valueList.length === 0) {
        return 0
    }
    return valueList.reduce(
        (minimumValue: number, currentValue: number): number => currentValue < minimumValue ? currentValue : minimumValue,
        valueList[0]
    )
}
const getMaximumValue = (valueList: number[]): number => {
    if (valueList.length === 0) {
        return 0
    }
    return valueList.reduce(
        (maximumValue: number, currentValue: number): number => currentValue > maximumValue ? currentValue : maximumValue,
        valueList[0]
    )
}
interface ICardState {
    isGestureHandlerLocked: boolean,
}

interface ICardProps {
    card: ICard,
    cardIndex: number,
    displayedCardIndex: number,
    cardsContainerDimensions: ICardsContainerDimensions,
    onMoveRight: (card: ICard) => void,
    onMoveLeft: (card: ICard) => void,
    isSelected: boolean,
}

interface IPosition {
    x: Animated.Node<number>,
    y: Animated.Node<number>,
}

interface ICardDimensions {
    left: Animated.Node<number>,
    top: Animated.Node<number>,
    scale: Animated.Node<number>,
}

interface IContainerAnimatedDimensions {
    width: Animated.Value<number>
    height: Animated.Value<number>
}

class Card extends React.PureComponent<ICardProps, ICardState> {
    containerAnimatedDimensions: IContainerAnimatedDimensions
    animatedActualCardIndex: Animated.Value<number>
    cardDimensions: ICardDimensions = {
        left: new Animated.Value(0),
        top: new Animated.Value(0),
        scale: new Animated.Value(1),
    }
    targetXValue: Animated.Value<number> = new Animated.Value(0)
    gesturePosition: IPosition = {
        x: new Animated.Value(0),
        y: new Animated.Value(0),
    }
    currentPosition: IPosition = {
        x: new Animated.Value(0),
        y: new Animated.Value(0),
    }
    gestureState: Animated.Value<GestureState> = new Animated.Value<GestureState>(GestureState.UNDETERMINED)

    constructor(props: ICardProps) {
        super(props)
        this.state = {
            isGestureHandlerLocked: false
        }
        this.containerAnimatedDimensions = {
            width: new Animated.Value(props.cardsContainerDimensions.width),
            height: new Animated.Value(props.cardsContainerDimensions.height),
        }

        this.animatedActualCardIndex = new Animated.Value(this.getActualCardIndex(props.cardIndex, props.displayedCardIndex))

        this.cardDimensions = this.setupCardDimensions(
            this.animatedActualCardIndex,
            this.containerAnimatedDimensions,
        )

        this.currentPosition.x = this.setupPositionX(this.gesturePosition.x, this.targetXValue, extremeXValues)
        this.currentPosition.y = this.setupPositionY(this.gesturePosition.y)
    }

    componentDidUpdate(prevProps: Readonly<ICardProps>): void {
        if (prevProps.displayedCardIndex !== this.props.displayedCardIndex) {
            this.updateAnimatedActualCardIndex()
            if (this.props.displayedCardIndex === this.props.cardIndex) {
                /** Undo was pressed */
                this.resetTargetXValue()
            }
            if (this.props.displayedCardIndex === this.props.cardIndex + 1) {
                /** LikeControl button pressed */
                if (this.props.isSelected) {
                    Animated.timing(this.targetXValue, {
                        toValue: getMaximumValue(extremeXValues),
                        duration: 0,
                        easing: Easing.linear,
                    }).start()
                } else {
                    Animated.timing(this.targetXValue, {
                        toValue: getMinimumValue(extremeXValues),
                        duration: 0,
                        easing: Easing.linear,
                    }).start()
                }
            }
        }
        if (prevProps.cardsContainerDimensions.height !== this.props.cardsContainerDimensions.height
            || prevProps.cardsContainerDimensions.width !== this.props.cardsContainerDimensions.width
        ) {
            this.updateContainerAnimatedDimensions()
        }
    }

    setupCardDimensions = (
        animatedActualCardIndex: Animated.Value<number>,
        containerAnimatedDimensions: IContainerAnimatedDimensions
    ): ICardDimensions => {
        const cardAspectRatio: Animated.Node<number> = divide(sub(containerAnimatedDimensions.height, (NUMBER_OF_CARDS_DISPLAYED - 1) * VERTICAL_OFFSET_BETWEEN_CARDS), containerAnimatedDimensions.width)
        const cardWidth: Animated.Node<number> = sub(containerAnimatedDimensions.width, multiply(
            HORIZONTAL_OFFSET_BETWEEN_CARDS * 2,
            animatedActualCardIndex,
        ))
        const cardHeight: Animated.Node<number> = multiply(cardWidth, cardAspectRatio)
        const left: Animated.Node<number> = multiply(HORIZONTAL_OFFSET_BETWEEN_CARDS, animatedActualCardIndex)

        const topByCardIndex: Animated.Node<number> = multiply(VERTICAL_OFFSET_BETWEEN_CARDS, sub(NUMBER_OF_CARDS_DISPLAYED, animatedActualCardIndex))
        const topScaleOffset: Animated.Node<number> = divide(sub(cardHeight, this.props.cardsContainerDimensions.height), 2)
        const top: Animated.Node<number> = add(topByCardIndex, topScaleOffset)
        const scale: Animated.Node<number> = divide(cardWidth, this.props.cardsContainerDimensions.width)
        return { left, top, scale }
    }

    updateContainerAnimatedDimensions = (): void => {
        const newContainerAnimatedDimensions: ICardsContainerDimensions = this.props.cardsContainerDimensions
        setTimeout(() => {
            Animated.timing(this.containerAnimatedDimensions.height, {
                toValue: newContainerAnimatedDimensions.height,
                duration: 0,
                easing: Easing.inOut(Easing.ease),
            }).start()
            Animated.timing(this.containerAnimatedDimensions.width, {
                toValue: newContainerAnimatedDimensions.width,
                duration: 0,
                easing: Easing.inOut(Easing.ease),
            }).start()
        }, 0)
    }

    updateAnimatedActualCardIndex = (): void => {
        const newActualCardIndex: number = this.getActualCardIndex(this.props.cardIndex, this.props.displayedCardIndex)
        setTimeout(() => {
            this.lockGestureHandler()
            Animated.timing(this.animatedActualCardIndex, {
                toValue: newActualCardIndex,
                duration: 300,
                easing: Easing.inOut(Easing.ease),
            }).start((data: { finished: boolean }) => {
                if (data.finished) {
                    this.unlockGestureHandler()
                }
            })
        }, 0)
    }

    lockGestureHandler = (): void => {
        this.setState({ isGestureHandlerLocked: true })
    }

    unlockGestureHandler = (): void => {
        this.setState({ isGestureHandlerLocked: false })
    }

    resetTargetXValue = (): void => {
        setTimeout(() => {
            Animated.timing(this.targetXValue, {
                toValue: 0,
                duration: 300,
                easing: Easing.inOut(Easing.ease),
            }).start()
        }, 0)
    }

    getActualCardIndex = (cardIndex: number, displayedCardIndex: number): number => cardIndex - displayedCardIndex

    setupPositionX = (
        value: Animated.Node<number>,
        targetXValue: Animated.Value<number>,
        extremeValues: number[],
    ): Animated.Node<number> => {
        const clock: Clock = new Clock()
        const state: Animated.SpringState = {
            position: new Animated.Value(0),
            finished: new Animated.Value(0),
            time: new Animated.Value(0),
            velocity: new Animated.Value(0),
        }
        const config: Animated.SpringConfig = {
            damping: 10,
            mass: 1,
            stiffness: 64,
            overshootClamping: 0,
            restSpeedThreshold: 0.01,
            restDisplacementThreshold: 0.01,
            toValue: targetXValue,
        }

        const endAnimation: Animated.Node<number> = block([
            cond(clockRunning(clock), 0,
                [
                    set(state.finished, 0),
                    set(state.time, 0),
                    set(state.velocity, 0),
                    startClock(clock),
                ]
            ),
            spring(clock, state, config),
            cond(state.finished, stopClock(clock)),
        ])

        const gestureEnd: Animated.Node<number> = block([
            cond(
                greaterThan(state.position, HORIZONTAL_MOVE_GAP),
                [
                    call([], this.onMoveRight),
                    set(targetXValue, getMaximumValue(extremeValues)),
                ],
            ),
            cond(
                lessThan(state.position, -HORIZONTAL_MOVE_GAP),
                [
                    call([], this.onMoveLeft),
                    set(targetXValue, getMinimumValue(extremeValues))
                ],
            ),
            set(this.gestureState, GestureState.UNDETERMINED),
        ])

        return block([
            cond(
                eq(this.gestureState, GestureState.ACTIVE),
                set(state.position, value),
                endAnimation,
            ),
            cond(
                eq(this.gestureState, GestureState.END),
                gestureEnd,
            ),
            state.position,
        ])
    }

    setupPositionY = (value: Animated.Node<number>): Animated.Node<number> => {
        const clock: Clock = new Clock()
        const state: Animated.SpringState = {
            position: new Animated.Value(0),
            finished: new Animated.Value(0),
            time: new Animated.Value(0),
            velocity: new Animated.Value(0),
        }
        const config: Animated.SpringConfig = {
            damping: 10,
            mass: 1,
            stiffness: 64,
            overshootClamping: 0,
            restSpeedThreshold: 0.01,
            restDisplacementThreshold: 0.01,
            toValue: new Animated.Value<number>(0),
        }
        return block([
            cond(
                eq(this.gestureState, GestureState.ACTIVE),
                set(state.position, value),
                [
                    cond(clockRunning(clock), 0,
                        [
                            set(state.finished, 0),
                            set(state.time, 0),
                            set(state.velocity, 0),
                            startClock(clock),
                        ]
                    ),
                    spring(clock, state, config),
                ],
            ),
            state.position,
        ])
    }

    onMoveRight = (): void => this.props.onMoveRight(this.props.card)
    onMoveLeft = (): void => this.props.onMoveLeft(this.props.card)

    onGestureEvent: (event: PanGestureHandlerGestureEvent) => void = event([
        {
            nativeEvent: {
                translationX: this.gesturePosition.x,
                translationY: this.gesturePosition.y,
                state: this.gestureState,
            },
        },
    ])

    render() {
        return (
            <PanGestureHandler
                onGestureEvent={this.onGestureEvent}
                onHandlerStateChange={this.onGestureEvent}
                enabled={this.props.cardIndex === this.props.displayedCardIndex && !this.state.isGestureHandlerLocked}
            >
                <Animated.View
                    style={[
                        styles.card,
                        {
                            zIndex: -this.props.cardIndex,
                            width: this.props.cardsContainerDimensions.width,
                            height: this.props.cardsContainerDimensions.height - VERTICAL_OFFSET_BETWEEN_CARDS * (NUMBER_OF_CARDS_DISPLAYED - 1),
                            top: this.cardDimensions.top,
                            opacity: interpolate(this.animatedActualCardIndex, {
                                inputRange: [-1, 0, NUMBER_OF_CARDS_DISPLAYED - 1, NUMBER_OF_CARDS_DISPLAYED],
                                outputRange: [0, 1, 1, 0],
                                extrapolate: Extrapolate.CLAMP,
                            }),

                            transform: [
                                {
                                    translateX: this.currentPosition.x,
                                    translateY: this.currentPosition.y,
                                    scale: this.cardDimensions.scale,
                                }
                            ],
                        }
                    ]}
                >
                    <View style={styles.cardContent}>
                        <View style={styles.overlay}>
                            {/** Using Image gradient for performance reason */}
                            <Image
                                resizeMode={'cover'}
                                style={styles.gradientImage}
                                source={gradientImage}
                            />
                        </View>
                        <Image
                            style={styles.cardImage}
                            source={{ uri: this.props.card.imageUrl }}
                        />
                        <View style={styles.textContainer}>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>{this.props.card.title}</Text>
                            </View>
                            <Text style={styles.description}>{this.props.card.description}</Text>
                            <Text style={styles.description}>{this.props.card.date.toLocaleString('en-US', dateOptions)}</Text>
                        </View>
                    </View>
                </Animated.View>
            </PanGestureHandler>
        )
    }
}
export default Card
