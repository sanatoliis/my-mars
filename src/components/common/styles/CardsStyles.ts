import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
    },
    cardsContainer: {
        flex: 1,
    },
    controlContainer: {
        position: 'absolute',
        left: 0,
        bottom: -28,
        right: 0,
    },
});

export default styles
