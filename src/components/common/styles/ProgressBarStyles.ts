import { StyleSheet } from 'react-native'
export const BAR_DIAMETER: number = 40

const styles = StyleSheet.create({
    progressBarContainer: {
        top: BAR_DIAMETER / 2,
        left: BAR_DIAMETER / 2,
    },
    image: {
        width: BAR_DIAMETER,
        height: BAR_DIAMETER,
    },
});

export default styles
