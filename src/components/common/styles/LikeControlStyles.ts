import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    controlContainer: {
        flex: 1,
        alignItems: 'center',
    },
    control: {
        height: 56,
        width: 56,
        borderRadius: 56,
        alignItems: 'center',
        justifyContent: 'center',

        shadowRadius: 24,
        shadowOffset: {
            width: 0,
            height: 16,
        },
        shadowColor: '#102027',
        shadowOpacity: 0.32,
    },
    likeControl: {
        paddingBottom: 4,
        backgroundColor: '#EB5757',
    },
    dislikeControl: {
        paddingTop: 4,
        backgroundColor: 'black',
    },
    image: {
        height: 24,
        width: 24,
    },
    disabledСontrol: {
        opacity: 0.2,
    },
});

export default styles
