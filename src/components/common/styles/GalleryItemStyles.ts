import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        height: 100,
        paddingHorizontal: 16,
        paddingVertical: 8,
        flexDirection: 'row',
        alignItems: 'stretch',
    },
    image: {
        width: 84,
    },
    description: {
        flex: 1,
        padding: 8,
    },
});

export default styles
