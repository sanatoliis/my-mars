import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    card: {
        position: 'absolute',
        borderRadius: 8,

        backgroundColor: 'white',
        shadowRadius: 24,
        shadowOffset: {
            width: 0,
            height: 16,
        },
        shadowColor: '#102027',
        shadowOpacity: 0.32,

    },
    cardContent: {
        flex: 1,
        borderRadius: 8,
        overflow: 'hidden',
    },
    cardImage: {
        flex: 1,
    },
    textContainer: {
        ...StyleSheet.absoluteFillObject,
        padding: 24,
        zIndex: 20,
    },
    titleContainer: {
        paddingBottom: 4,
    },
    title: {
        fontFamily: 'IBMPlexSansMedium',
        color: 'white',
        fontSize: 20,
        lineHeight: 28,
        letterSpacing: 0.15,
    },
    description: {
        fontFamily: 'IBMPlexSansMedium',
        color: 'white',
        fontSize: 14,
        lineHeight: 20,
        letterSpacing: 0.75,
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        zIndex: 10,
    },
    gradientImage: {
        width: '100%',
        height: '100%',
    },
});

export default styles
