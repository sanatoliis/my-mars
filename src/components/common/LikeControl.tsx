import React from 'react'
import { View, Image, ImageSourcePropType } from 'react-native'
import Animated, {
    event,
    set,
    cond,
    eq,
    call,
    Easing,
    block,
    Clock,
    startClock,
    clockRunning,
    stopClock,
    timing,
} from 'react-native-reanimated'
import {
    State as GestureState,
    TapGestureHandler,
    TapGestureHandlerGestureEvent,
} from 'react-native-gesture-handler'
import styles from './styles/LikeControlStyles'

const likeImageSource: ImageSourcePropType = require('../../assets/icons/ico-thumbs-up.png')
const dislikeImageSource: ImageSourcePropType = require('../../assets/icons/ico-thumbs-down.png')

const ANIMATION_DURATION: number = 100
const MIN_SCALE_VALUE: number = 1
const MAX_SCALE_VALUE: number = 1.29

interface ILikeControlProps {
    isEnabled: boolean,
    onLike: () => void,
    onDislike: () => void,
}

const getOnGestureEvent = (gestureState: Animated.Value<GestureState>): ((event: TapGestureHandlerGestureEvent) => void) => {
    return event([
        {
            nativeEvent: {
                state: gestureState,
            },
        },
    ])
}

const configScaleValue = (
    clock: Clock,
    gestureState: Animated.Value<GestureState>,
    onTap: () => void,
): Animated.Node<number> => {
    const timingState: Animated.TimingState = {
        frameTime: new Animated.Value(0),
        finished: new Animated.Value(0),
        position: new Animated.Value(MIN_SCALE_VALUE),
        time: new Animated.Value(0),
    }
    const toValue: Animated.Value<number> = new Animated.Value<number>(MIN_SCALE_VALUE)
    const duration: Animated.Value<number> = new Animated.Value<number>(ANIMATION_DURATION)
    const timingConfig: Animated.TimingConfig = {
        toValue: toValue,
        duration: duration,
        easing: Easing.inOut(Easing.ease),
    }
    const resetState: Animated.Node<number> = block([
        set(timingState.finished, 0),
        set(timingState.frameTime, 0),
        set(timingState.time, 0),
    ])
    const reset: Animated.Node<number> = cond(clockRunning(clock), 0,
        [
            resetState,
            startClock(clock),
        ]
    )
    return block([
        cond(
            eq(gestureState, GestureState.BEGAN),
            set(toValue, MAX_SCALE_VALUE),
        ),
        cond(
            eq(gestureState, GestureState.END),
            [
                call([], onTap),
                set(toValue, MIN_SCALE_VALUE),
                set(gestureState, GestureState.UNDETERMINED),
            ],
        ),
        cond(
            eq(gestureState, GestureState.FAILED),
            set(toValue, MIN_SCALE_VALUE),
        ),
        reset,
        timing(clock, timingState, timingConfig),
        cond(timingState.finished, stopClock(clock)),
        timingState.position,
    ])
}

const dislikeСlock: Animated.Clock = new Animated.Clock()
const likeСlock: Animated.Clock = new Animated.Clock()
const dislikeGestureState: Animated.Value<GestureState> = new Animated.Value<GestureState>(GestureState.UNDETERMINED)
const likeGestureState: Animated.Value<GestureState> = new Animated.Value<GestureState>(GestureState.UNDETERMINED)

const LikeControl: React.FC<ILikeControlProps> = (props: ILikeControlProps) => {

    const dislikeScaleValue: Animated.Node<number> = configScaleValue(dislikeСlock, dislikeGestureState, props.onDislike)
    const likeScaleValue: Animated.Node<number> = configScaleValue(likeСlock, likeGestureState, props.onLike)

    return (
        <View style={styles.container} pointerEvents={'box-none'}>
            <View style={styles.controlContainer} pointerEvents={'box-none'}>
                <TapGestureHandler
                    enabled={props.isEnabled}
                    numberOfTaps={1}
                    onGestureEvent={getOnGestureEvent(dislikeGestureState)}
                    onHandlerStateChange={getOnGestureEvent(dislikeGestureState)}
                    shouldCancelWhenOutside
                >
                    <Animated.View
                        style={[
                            styles.control,
                            styles.dislikeControl,
                            {
                                transform: [
                                    {
                                        scale: dislikeScaleValue,
                                    }
                                ]
                            },
                            !props.isEnabled && styles.disabledСontrol,
                        ]}
                    >
                        <Image
                            style={styles.image}
                            source={dislikeImageSource}
                        />
                    </Animated.View>
                </TapGestureHandler>
            </View>
            <View style={styles.controlContainer} pointerEvents={'box-none'}>
                <TapGestureHandler
                    enabled={props.isEnabled}
                    onGestureEvent={getOnGestureEvent(likeGestureState)}
                    onHandlerStateChange={getOnGestureEvent(likeGestureState)}
                    shouldCancelWhenOutside
                >
                    <Animated.View
                        style={[
                            styles.control,
                            styles.likeControl,
                            {
                                transform: [
                                    {
                                        scale: likeScaleValue,
                                    }
                                ]
                            },
                            !props.isEnabled && styles.disabledСontrol,
                        ]}>
                        <Image
                            style={styles.image}
                            source={likeImageSource}
                        />
                    </Animated.View>
                </TapGestureHandler>
            </View>
        </View>
    )
}

export default LikeControl
