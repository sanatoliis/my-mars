import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './styles/GalleryItemStyles'

export interface IGalleryItemContainerDimensions {
    width: number,
    height: number,
}

export interface IProps {
    imageUrl: string,
    title: string,
    description: string,
    date: Date,
}

const GalleryItem: React.FC<IProps> = (props: IProps) => {
    return (
        <View style={styles.container}>
            <Image
                style={styles.image}
                source={{ uri: props.imageUrl }}
            />
            <View style={styles.description}>
                <Text>{props.title}</Text>
                <Text>{props.description}</Text>
                <Text>{props.date.toLocaleDateString()}</Text>
            </View>
        </View>
    )
}

export default GalleryItem
