import React, { useState, useEffect } from 'react'
import { View, LayoutChangeEvent } from 'react-native'
import Card from './Card'
import styles from './styles/CardsStyles'
import Animated, { Easing } from 'react-native-reanimated'
import { ICard } from '../../app'
import LikeControl from './LikeControl'

const LAYOUT_TIMEOUT: number = 200
const LAYOUT_OPACITY_DELAY: number = 2000

export interface ICardsContainerDimensions {
    width: number,
    height: number,
}

export interface IProps {
    cardList: ICard[],
    selectedCardIdList: string[],
    onLike: (card: ICard) => void,
    onDislike: (card: ICard) => void,
    displayedCardIndex: number,
    isEnabled: boolean,
}

const animatedOpacity: Animated.Value<number> = new Animated.Value<number>(0)

const Cards: React.FC<IProps> = (props: IProps) => {
    const [cardsContainerDimensions, setCardsContainerDimensions] = useState<ICardsContainerDimensions>({
        width: 0,
        height: 0,
    })
    const [isLayoutTimeoutLeft, setIsLayoutTimeoutLeft] = useState<boolean>(false)

    useEffect(() => {
        const layoutTimer: NodeJS.Timeout = setTimeout(() => {
            setIsLayoutTimeoutLeft(true)
        }, LAYOUT_TIMEOUT)

        const layoutOpacityTimer: NodeJS.Timeout = setTimeout(() => {
            Animated.timing(animatedOpacity, {
                toValue: 1,
                duration: 300,
                easing: Easing.inOut(Easing.ease),
            }).start()
        }, LAYOUT_OPACITY_DELAY)

        return () => {
            clearTimeout(layoutTimer)
            clearTimeout(layoutOpacityTimer)
        }
    }, [])

    const isContainerMeasured: boolean = cardsContainerDimensions.height !== 0 && cardsContainerDimensions.width !== 0

    const onLayout = (event: LayoutChangeEvent): void => {
        if (event.nativeEvent.layout.width !== 0
            && event.nativeEvent.layout.height !== 0
            && (event.nativeEvent.layout.width !== cardsContainerDimensions.width
                || event.nativeEvent.layout.height !== cardsContainerDimensions.height
            )
        ) {
            setCardsContainerDimensions({
                width: event.nativeEvent.layout.width,
                height: event.nativeEvent.layout.height,
            })
        }
    }

    const renderItem = (card: ICard, index: number): React.ReactElement<Card> => {
        const isSelected: boolean = props.selectedCardIdList.includes(card.id)
        return (
            <Card
                key={card.id}
                card={card}
                cardIndex={index}
                displayedCardIndex={props.displayedCardIndex}
                cardsContainerDimensions={cardsContainerDimensions}
                onMoveRight={props.onLike}
                onMoveLeft={props.onDislike}
                isSelected={isSelected}
            />
        )
    }

    const displayedCard: ICard = props.cardList[props.displayedCardIndex]

    return (
        <View style={styles.container}>
            <Animated.View
                style={[styles.cardsContainer, { opacity: animatedOpacity }]}
                onLayout={onLayout}
            >
                {isContainerMeasured && isLayoutTimeoutLeft ? props.cardList.map(renderItem) : null}
                <View style={styles.controlContainer} pointerEvents={'box-none'}>
                    <LikeControl
                        isEnabled={props.displayedCardIndex < props.cardList.length && props.isEnabled}
                        onLike={() => props.onLike(displayedCard)}
                        onDislike={() => props.onDislike(displayedCard)}
                    />
                </View>
            </Animated.View>
        </View>
    )
}

export default Cards
