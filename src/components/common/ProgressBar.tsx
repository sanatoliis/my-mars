import React, { useEffect } from 'react'
import { View, Image, ImageSourcePropType } from 'react-native'
import Animated, { Easing } from 'react-native-reanimated'

import styles, { BAR_DIAMETER } from './styles/ProgressBarStyles'

const progressBarIconSource: ImageSourcePropType = require('../../assets/icons/progress-bar.png')

const ANIMATION_DURATION: number = 800

const configRotateValue = (clock: Animated.Clock): Animated.Node<number> => {
    const timingState: Animated.TimingState = {
        frameTime: new Animated.Value(0),
        finished: new Animated.Value(0),
        position: new Animated.Value(0),
        time: new Animated.Value(0),
    }
    const timingConfig: Animated.TimingConfig = {
        toValue: new Animated.Value(360),
        duration: ANIMATION_DURATION,
        easing: Easing.linear,
    }
    const resetState = Animated.block([
        Animated.set(timingState.position, 0),
        Animated.set(timingState.finished, 0),
        Animated.set(timingState.frameTime, 0),
    ])
    return Animated.block([
        Animated.startClock(clock),
        Animated.timing(clock, timingState, timingConfig),
        Animated.cond(timingState.finished, resetState),
        timingState.position,
    ])
}

interface IProps {

}

const ProgressBar: React.FC<IProps> = (props: IProps) => {
    const clock: Animated.Clock = new Animated.Clock()
    useEffect(() => {
        return () => {
            Animated.stopClock(clock)
        }
    }, [])
    const rotateValue: Animated.Node<number> = configRotateValue(clock)
    return (
        <View style={styles.progressBarContainer}>
            <Animated.View
                style={{
                    transform: [
                        {
                            translateX: -BAR_DIAMETER / 2,
                            translateY: -BAR_DIAMETER / 2,
                        },
                        {
                            rotate: Animated.interpolate(rotateValue, {
                                inputRange: [0, 360],
                                outputRange: ['0deg', '360deg'],
                            }),
                        }
                    ]
                }}
            >
                <Image style={styles.image} source={progressBarIconSource} />
            </Animated.View>
        </View>
    )
}

export default ProgressBar
