import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
    },
    header: {
        height: 56,
        flexDirection: 'row',
        alignItems: 'stretch',
        paddingHorizontal: 16,
    },
    sideContent: {
        width: 60,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    centerContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerButton: {
        fontFamily: 'IBMPlexSansMedium',
        fontSize: 16,
        lineHeight: 20,
        letterSpacing: 0.25,
        color: '#EB5757',
    },
    headerTitle: {
        fontFamily: 'IBMPlexSansMedium',
        color: '#102027',
        fontSize: 18,
        lineHeight: 24,
        letterSpacing: 0.5,
    },
});

export default styles
