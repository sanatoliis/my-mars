import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        height: 56,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 16,
    },
    body: {
        flex: 1,
    },
    likeImage: {
        height: 24,
        width: 24,
    },
    leftHeaderContainer: {
        width: 60,
        alignItems: 'flex-start'
    },
    rightHeaderContainer: {
        width: 60,
        alignItems: 'flex-end'
    },
    centerHeaderContainer: {
        flex: 1,
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: 'IBMPlexSansMedium',
        color: '#102027',
        fontSize: 18,
        lineHeight: 24,
        letterSpacing: 0.5,
    },
    headerButton: {
        fontFamily: 'IBMPlexSansMedium',
        fontSize: 16,
        lineHeight: 20,
        letterSpacing: 0.25,
    },
    headerDisabledButton: {
        color: '#CFD8DC',
    },
    headerEnabledButton: {
        color: '#EB5757',
    },
    cardsLeftLabelContainer: {
        paddingTop: 16,
        alignItems: 'center',
        zIndex: -10,
    },
    loaderContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    caption: {
        fontFamily: 'IBMPlexSansMedium',
        color: '#727C81',
        fontSize: 14,
        lineHeight: 20,
        letterSpacing: 0.75,
    },
    errorContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
    }
});

export default styles
