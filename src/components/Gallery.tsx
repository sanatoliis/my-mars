import React from 'react'
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    SafeAreaView,
} from 'react-native'
import styles from './styles/GalleryStyles'
import { INavigation, ICard } from '../app'
import GalleryItem from './common/GalleryItem'

const renderGalleryItem = (card: ICard): React.ReactElement<typeof GalleryItem> => {
    return (
        <GalleryItem
            key={card.id}
            imageUrl={card.imageUrl}
            title={card.title}
            description={card.description}
            date={card.date}
        />
    )
}

interface IGalleryProps {
    cardList: ICard[],
    navigation: INavigation,
}

const Gallery: React.FC<IGalleryProps> = (props: IGalleryProps) => {
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <View style={styles.sideContent}>
                    <TouchableOpacity
                        onPress={() => props.navigation.goBack()}
                    >
                        <Text style={styles.headerButton}>Back</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.centerContent}>
                    <Text style={styles.headerTitle}>My gallery</Text>
                </View>
                <View style={styles.sideContent} />
            </View>
            <ScrollView>
                {props.cardList.map(renderGalleryItem)}
            </ScrollView>
        </SafeAreaView>
    )
}

export default Gallery
