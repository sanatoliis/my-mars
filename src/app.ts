import { NavigationProp } from '@react-navigation/native'

export namespace Enums {
    export type RouteType = (
        'Evaluating'
        | 'Gallery'
    )
    export type IPhase = (
        'InProgress'
        | 'Success'
        | 'Failure'
        | 'Never'
    )
}

export interface IAppState {
    cardList: ICard[],
    selectedCardIdList: string[],
    callPostNasaRoverPhotosPhase: Enums.IPhase,
    callPostNasaRoverPhotosError: IError | null,
    displayedCardIndex: number,
}

export interface INavigation extends NavigationProp<{ [key in Enums.RouteType]: any }> { }

export interface ICard {
    id: string,
    imageUrl: string,
    title: string,
    description: string,
    date: Date,
}

export interface IEvaluatingProps {
    cardList: ICard[],
    selectedCardIdList: string[],
    navigation: INavigation,
    addCardToSelected: (cardId: string) => void,
    removeCardFromSelected: (cardId: string) => void,
    callPostNasaRoverPhotosPhase: Enums.IPhase,
    displayedCardIndex: number,
    setDisplayedCardIndex: (newCardIndex: number) => void,
    callPostNasaRoverPhotosError: IError | null,
}

export interface IGalleryProps {
    cardList: ICard[],
    navigation: INavigation,
}

export interface IError {
    title: string,
    message: string,
    status: number,
}

export interface ICamera {
    id: number,
    name: string,
    rover_id: number,
    full_name: string,
}

export interface IRover {
    id: number,
    name: string,
    landing_date: string, // "2012-08-06"
    launch_date: string, // "2011-11-26"
    status: string,
}

export interface IPhoto {
    id: number,
    sol: number,
    camera: ICamera,
    img_src: string,
    earth_date: string, // "2015-05-30"
    rover: IRover,
}

export interface IICallPostNasaRoverPhotosData {
    photos: IPhoto[],
}

export interface IAxiosError {
    code?: string,
    message?: string,
}

export interface IAxiosErrorData {
    error?: IAxiosError,
    status: number,
}
