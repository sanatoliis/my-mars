import Redux, { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import reducer from './reducer'

declare const __DEV__: boolean

const middleware: Redux.Middleware[] = [thunk]

const enhancer = __DEV__ ? composeWithDevTools(applyMiddleware(...middleware)) : applyMiddleware(...middleware)

const store = createStore(
    reducer,
    enhancer,
)

export default store
