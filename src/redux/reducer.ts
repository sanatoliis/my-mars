import { handleActions, Action, BaseAction, ReduxCompatibleReducer } from 'redux-actions'
import * as actionsEvaluating from './actions/ActionsEvaluating'
import { IAppState } from '../app'
import { initialState } from '../models/Models'

const handleActionsCustom: any = handleActions

const reducer: ReduxCompatibleReducer<IAppState, any> = handleActionsCustom({
    [actionsEvaluating.PERFORM_CONTAINER_INIT]: (state: IAppState, action: BaseAction): IAppState => {
        return {
            ...state,
        }
    },
    [actionsEvaluating.ADD_CARD_TO_SELECTED]: (state: IAppState, action: Action<actionsEvaluating.ADD_CARD_TO_SELECTED_PAYLOAD>): IAppState => {
        return {
            ...state,
            selectedCardIdList: [...state.selectedCardIdList, action.payload.cardId]
        }
    },
    [actionsEvaluating.REMOVE_CARD_FROM_SELECTED]: (state: IAppState, action: Action<actionsEvaluating.REMOVE_CARD_FROM_SELECTED_PAYLOAD>): IAppState => {
        return {
            ...state,
            selectedCardIdList: state.selectedCardIdList.filter((cardId: string): boolean => cardId !== action.payload.cardId)
        }
    },
    [actionsEvaluating.CALL_POST_NASA_ROVER_PHOTOS]: (state: IAppState, action: BaseAction): IAppState => {
        return {
            ...state,
        }
    },
    [actionsEvaluating.CALL_POST_NASA_ROVER_PHOTOS_REQUEST]: (state: IAppState, action: BaseAction): IAppState => {
        return {
            ...state,
            callPostNasaRoverPhotosPhase: 'InProgress'
        }
    },
    [actionsEvaluating.CALL_POST_NASA_ROVER_PHOTOS_SUCCESS]: (state: IAppState, action: Action<actionsEvaluating.CALL_POST_NASA_ROVER_PHOTOS_SUCCESS_PAYLOAD>): IAppState => {
        return {
            ...state,
            callPostNasaRoverPhotosPhase: 'Success',
            cardList: action.payload.cardList
        }
    },
    [actionsEvaluating.CALL_POST_NASA_ROVER_PHOTOS_FAILURE]: (state: IAppState, action: Action<actionsEvaluating.CALL_POST_NASA_ROVER_PHOTOS_FAILURE_PAYLOAD>): IAppState => {
        return {
            ...state,
            callPostNasaRoverPhotosPhase: 'Failure',
            callPostNasaRoverPhotosError: action.payload.error
        }
    },
    [actionsEvaluating.SET_DISPLAYED_CARD_INDEX]: (state: IAppState, action: Action<actionsEvaluating.SET_DISPLAYED_CARD_INDEX_PAYLOAD>): IAppState => {
        return {
            ...state,
            displayedCardIndex: action.payload.newCardIndex,
        }
    },
}, initialState)

export default reducer
