import { Action, BaseAction } from 'redux-actions'
import { ICard, IError } from '../../app'

export const PERFORM_CONTAINER_INIT: string = 'PERFORM_CONTAINER_INIT'
export const ADD_CARD_TO_SELECTED: string = 'ADD_CARD_TO_SELECTED'
export const REMOVE_CARD_FROM_SELECTED: string = 'REMOVE_CARD_FROM_SELECTED'
export const CALL_POST_NASA_ROVER_PHOTOS: string = 'CALL_POST_NASA_ROVER_PHOTOS'
export const CALL_POST_NASA_ROVER_PHOTOS_REQUEST: string = 'CALL_POST_NASA_ROVER_PHOTOS_REQUEST'
export const CALL_POST_NASA_ROVER_PHOTOS_SUCCESS: string = 'CALL_POST_NASA_ROVER_PHOTOS_SUCCESS'
export const CALL_POST_NASA_ROVER_PHOTOS_FAILURE: string = 'CALL_POST_NASA_ROVER_PHOTOS_FAILURE'
export const SET_DISPLAYED_CARD_INDEX: string = 'SET_DISPLAYED_CARD_INDEX'

export type PERFORM_CONTAINER_INIT_PAYLOAD = {}
export const performContainerInit = (): BaseAction => ({ type: PERFORM_CONTAINER_INIT })

export type ADD_CARD_TO_SELECTED_PAYLOAD = { cardId: string }
export const addCardToSelected = (cardId: string): Action<ADD_CARD_TO_SELECTED_PAYLOAD> =>
    ({ type: ADD_CARD_TO_SELECTED, payload: { cardId } })

export type REMOVE_CARD_FROM_SELECTED_PAYLOAD = { cardId: string }
export const removeCardFromSelected = (cardId: string): Action<REMOVE_CARD_FROM_SELECTED_PAYLOAD> =>
    ({ type: REMOVE_CARD_FROM_SELECTED, payload: { cardId } })

export type CALL_POST_NASA_ROVER_PHOTOS_PAYLOAD = {}
export const callPostNasaRoverPhotos = (): BaseAction => ({ type: CALL_POST_NASA_ROVER_PHOTOS })

export type CALL_POST_NASA_ROVER_PHOTOS_REQUEST_PAYLOAD = {}
export const callPostNasaRoverPhotosRequest = (): BaseAction => ({ type: CALL_POST_NASA_ROVER_PHOTOS_REQUEST })

export type CALL_POST_NASA_ROVER_PHOTOS_SUCCESS_PAYLOAD = { cardList: ICard[] }
export const callPostNasaRoverPhotosSuccess = (cardList: ICard[]): Action<CALL_POST_NASA_ROVER_PHOTOS_SUCCESS_PAYLOAD> => ({ type: CALL_POST_NASA_ROVER_PHOTOS_SUCCESS, payload: { cardList } })

export type CALL_POST_NASA_ROVER_PHOTOS_FAILURE_PAYLOAD = { error: IError }
export const callPostNasaRoverPhotosFailure = (error: IError): Action<CALL_POST_NASA_ROVER_PHOTOS_FAILURE_PAYLOAD> => ({ type: CALL_POST_NASA_ROVER_PHOTOS_FAILURE, payload: { error } })

export type SET_DISPLAYED_CARD_INDEX_PAYLOAD = { newCardIndex: number }
export const setDisplayedCardIndex = (newCardIndex: number): Action<SET_DISPLAYED_CARD_INDEX_PAYLOAD> =>
    ({ type: SET_DISPLAYED_CARD_INDEX, payload: { newCardIndex } })
