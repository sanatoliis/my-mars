import { createSelector } from 'reselect'
import { IAppState, ICard } from '../app'

export const getCardList = (state: IAppState): ICard[] => state.cardList
export const getSelectedCardIdList = (state: IAppState): string[] => state.selectedCardIdList

export const selectedCardsSelector = createSelector<IAppState, ICard[], string[], ICard[]>(
    getCardList,
    getSelectedCardIdList,
    (cardList: ICard[], selectedCardIdList: string[]): ICard[] => {
        return cardList.filter((card: ICard): boolean => selectedCardIdList.includes(card.id))
    }
)
