import { IAppState } from '../app'

export const initialState: IAppState = {
    cardList: [],
    selectedCardIdList: [],
    callPostNasaRoverPhotosPhase: 'Never',
    callPostNasaRoverPhotosError: null,
    displayedCardIndex: 0,
}
