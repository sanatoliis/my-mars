import { IAppState, ICard, IError, IICallPostNasaRoverPhotosData } from "../app";
import * as actionsEvaluating from '../redux/actions/ActionsEvaluating'
import axios, { AxiosResponse } from 'axios'
import { convertCallPostError, convertCallPostNasaRoverPhotosResponse } from "../Utils/util";

export const performContainerInit = () => (dispatch: Function, getState: () => IAppState) => {
    dispatch(actionsEvaluating.performContainerInit())
    const state: IAppState = getState()

    if (state.callPostNasaRoverPhotosPhase === 'Never') {
        dispatch(callPostNasaRoverPhotos())
    }
}

export const addCardToSelected = (cardId: string) => (dispatch: Function, getState: () => IAppState) => {
    dispatch(actionsEvaluating.addCardToSelected(cardId))
}

export const removeCardFromSelected = (cardId: string) => (dispatch: Function, getState: () => IAppState) => {
    dispatch(actionsEvaluating.removeCardFromSelected(cardId))
}

export const callPostNasaRoverPhotos = () => (dispatch: Function, getState: () => IAppState) => {
    dispatch(actionsEvaluating.callPostNasaRoverPhotos())

    dispatch(callPostNasaRoverPhotosRequest())
}

export const callPostNasaRoverPhotosRequest = () => async (dispatch: Function, getState: () => IAppState) => {
    dispatch(actionsEvaluating.callPostNasaRoverPhotosRequest())

    try {
        const response: AxiosResponse<IICallPostNasaRoverPhotosData> = await axios.get('https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&page=1&api_key=hTZkXHTOye8Ua2XdWHDY5LccsHagK49ygUaYoq0B')
        const cardList: ICard[] = convertCallPostNasaRoverPhotosResponse(response)
        dispatch(callPostNasaRoverPhotosSuccess(cardList))
    } catch (error) {
        dispatch(callPostNasaRoverPhotosFailure(convertCallPostError(error)))
    }
}

export const callPostNasaRoverPhotosSuccess = (cardList: ICard[]) => (dispatch: Function, getState: () => IAppState) => {
    dispatch(actionsEvaluating.callPostNasaRoverPhotosSuccess(cardList))
}

export const callPostNasaRoverPhotosFailure = (error: IError) => (dispatch: Function, getState: () => IAppState) => {
    dispatch(actionsEvaluating.callPostNasaRoverPhotosFailure(error))
}

export const setDisplayedCardIndex = (newCardIndex: number) => (dispatch: Function, getState: () => IAppState) => {
    dispatch(actionsEvaluating.setDisplayedCardIndex(newCardIndex))
}
